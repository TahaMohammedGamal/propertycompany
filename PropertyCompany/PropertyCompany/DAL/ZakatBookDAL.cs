﻿using PropertyCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PropertyCompany.DAL
{
    public class ZakatBookDAL
    {
        public static int AddNewZakatBook(ZakatBook ZB)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.ZakatBooks.Add(ZB);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Task<int> AddNewZakatBookAsync(ZakatBook ZK)
        {
            return Task.Run(() => AddNewZakatBook(ZK));
        }
    }
}