﻿using PropertyCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace PropertyCompany.DAL
{
    public class PersonalWithdrawalsBookDAL
    {
        public static int AddNewPersonalWithdrawalsBook(PersonalWithdrawalsBook PWD)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.PersonalWithdrawalsBooks.Add(PWD);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static Task<int> AddNewPersonalWithdrawalsBookAsync(PersonalWithdrawalsBook PWD)
        {
            return Task.Run(() => AddNewPersonalWithdrawalsBook(PWD));
        }
    }
}