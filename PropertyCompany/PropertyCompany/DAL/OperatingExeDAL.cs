﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using PropertyCompany.HelperClasses;
using PropertyCompany.Models;

namespace PropertyCompany.DAL
{
    public class OperatingExeDAL
    {
        public static int AddNewOperatingExeBook(ExpensesBook nwBook)
        {
            try
            {
                using (PropertyCompanyEntities dbcontext = new PropertyCompanyEntities())
                {
                    dbcontext.ExpensesBooks.Add(nwBook);
                    if (dbcontext.SaveChanges() > 0)
                        return 1;

                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
 

        public static Task<int> AddNewOperatingExeBookAsync(ExpensesBook nwBook)
        {
            return Task.Run(() => AddNewOperatingExeBook(nwBook));
        }
        
    }
}