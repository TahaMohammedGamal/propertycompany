﻿function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
var ShowHideProgress = (function () {
    "use strict";
    return {
        ShowProgress: (function () {
            sleep(2000);
            if ($scope.boollvar == == true) {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
            if ($scope.boollvar == false ) {
                var loading = $(".loading");
                loading.hide();
            }
        }())
    };
}());
