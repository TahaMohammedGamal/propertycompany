﻿/// <reference path="jquery-1.10.2.min.js" />

//var _EnableDebug = false;

//function _SetBreakPointHere()
//{
//    if (_EnableDebug)
//        debugger;
//}

//function EnableDebug(isEnabled)
//{
//    _EnableDebug = isEnabled;
//}

////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////
// this function hide all divs with class .fms-panel and show only div with id's passed to function args
// using could be like General_ShowDiv("#id1", "#id2",.....
function General_ShowDiv() {
    $(".agz-panel").hide();
    var args = [];
    for (var i = 0; i < arguments.length; ++i) {
        args[i] = arguments[i];
        $(args[i]).show();
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  wait modal function 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

function SetMessageBox(title, text) {
    document.getElementById("alertTitle").innerHTML = title;
    document.getElementById("messageText").innerHTML = text;
}
function SetRedirectLink(link) {
    if (link == "") {
        $("#OkButton").click(function () {
            $("#alertModal").modal('hide');
            location.reload();
        });
    }
    else {
        $("#OkButton").prop("href", link);
    }
}

function ShowMessageBox() {
    $('#alertModal').modal('show');
}

function ShowAndSetMessageBox(title, text, isError, CallBack) {


    if (typeof isError == 'undefined')
        isError = false;

    $("#alertModal_OkButton").unbind('click');

    document.getElementById("alertTitle").innerHTML = title;
    document.getElementById("messageText").innerHTML = text;

    if (isError) {
        $("#alertTitle").addClass("text-danger");
        $("#messageText").addClass("text-danger");
        $("#alertModal_OkButton").removeClass("btn-primary").addClass("btn-danger");
    }
    else {
        $("#alertTitle").removeClass("text-danger");
        $("#messageText").removeClass("text-danger");
        $("#alertModal_OkButton").removeClass("btn-danger").addClass("btn-primary");
    }

    if (typeof CallBack !== 'undefined' && CallBack != null)
        $("#alertModal_OkButton").on("click", function () {
            CallBack();
        });


    $('#alertModal').modal('show');

}


function ShowWaitDialog(waitmsg) {
    waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
    $("#wait-modal-progress-msg").text(waitmsg);
    $("#pleaseWaitDialog").modal();
}

function HideWaitDialog() {
    //if ($('#pleaseWaitDialog').hasClass('in')) //Is wait dailog open
    $("#pleaseWaitDialog").modal('hide');
}

function UpdateWaitDailogMsg(waitmsg) {
    waitmsg = typeof waitmsg !== 'undefined' ? waitmsg : "Please wait ...";
    $("#wait-modal-progress-msg").text(waitmsg);
}


IsFMSModalConfirmationPluginInit = false;
function ShowConfirmationModal(title, msg, isDelete, callBackResult, OkLabel, CancelLabel, hasComment, PlaceHolder) {

    //OkLabel = typeof OkLabel !== 'undefined' ? OkLabel : "Yes";
    //CancelLabel = typeof CancelLabel !== 'undefined' ? CancelLabel : "No";
    $("#modal-confirmation-plugin-comment-area").val("");
    $("#modal-confirmation-plugin-lbl").text(title);
    $("#modal-confirmation-plugin-message").text(msg);


    //if (!IsFMSModalConfirmationPluginInit)
    {
        $("#modal-confirmation-plugin-btn-yes").unbind('click');
        $("#modal-confirmation-plugin-btn-yes").on("click", function () {
            callBackResult(true);
        });

        $("#modal-confirmation-plugin-btn-no").unbind('click');
        $("#modal-confirmation-plugin-btn-no").on("click", function () {
            callBackResult(false);
        });
        IsFMSModalConfirmationPluginInit = true;
    }

    if (typeof OkLabel == 'undefined' && isDelete) {
        $("#modal-confirmation-plugin-btn-yes").text("Delete");
    }
    else if (typeof OkLabel == 'undefined' && !isDelete)
        $("#modal-confirmation-plugin-btn-yes").text("Yes");
    else if (typeof OkLabel !== 'undefined')
        $("#modal-confirmation-plugin-btn-yes").text(OkLabel);
    else
        $("#modal-confirmation-plugin-btn-yes").text("Yes");

    if (typeof CancelLabel !== 'undefined')
        $("#modal-confirmation-plugin-btn-no").text(CancelLabel);
    else
        $("#modal-confirmation-plugin-btn-no").text("No");


    if (isDelete) {
        $("#modal-confirmation-plugin-btn-yes").removeClass("btn-primary").addClass("btn-danger");
    }
    else {
        $("#modal-confirmation-plugin-btn-yes").removeClass("btn-danger").addClass("btn-primary");
    }

    if (typeof hasComment !== 'undefined') {
        if (hasComment) {
            $("#modal-confirmation-plugin-comment-area").show();
        }
        else {
            $("#modal-confirmation-plugin-comment-area").hide();
        }
    }
    else {
        $("#modal-confirmation-plugin-comment-area").hide();
    }

    if (typeof PlaceHolder !== 'undefined') {
        $("#modal-confirmation-plugin-comment-area").attr('placeholder', PlaceHolder);
    }
    else {
        PlaceHolder = "Please state the reasons for order cancelation"
        $("#modal-confirmation-plugin-comment-area").attr('placeholder', PlaceHolder);
    }
    $("#modal-confirmation-plugin").modal("show");

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// function to check if the function initialized with datatables plugin or not
function General_isDataTable(nTable) {
    var settings = $.fn.dataTableSettings;
    for (var i = 0, iLen = settings.length ; i < iLen ; i++) {
        if (settings[i].nTable == nTable) {
            return true;
        }
    }
    return false;
}
// this function clear data table from its data 
function General_ClearDataTable(tableid) {
    if (General_isDataTable($(tableid)[0])) {
        var table = $(tableid).DataTable();
        table.destroy();
        $(tableid).empty();
    }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// autosearch 
(function ($) {
    $.fn.AutoSearchInputTextAjax = function (url, CallBackResult) {
        var bRequestInProgress = false;
        var self = this;
        $(this).on("propertychange change keyup paste input", function () {
            if ($(this).val() == "") {
                CallBackResult(null);
            }
            if ($(this).val().length >= 3) {
                if (!bRequestInProgress)
                    AutoSearchInputTextAjaxCall($(this).val());
            }
            else {
                CallBackResult(null);
            }
        });

        function AutoSearchInputTextAjaxCall(searchTerm) {
            var uri = url + "/" + searchTerm;
            bRequestInProgress = true;
            $.ajax(
                {
                    url: uri,
                    type: 'get',
                    dataType: 'json',
                    success: function (result) {
                        CallBackResult(result);
                        bRequestInProgress = false;
                        if ($(self).val() == "") {
                            CallBackResult(null);
                        }
                        if (($(self).val().length >= 3)) {
                            if ($(self).val() != searchTerm)
                                AutoSearchInputTextAjaxCall($(self).val());
                            else
                                return;
                        }
                        else {
                            CallBackResult(null);
                        }
                    },
                    error: function (x, y, z) {
                        CallBackResult(null);
                        bRequestInProgress = false;
                    }
                });

        }
    }
})(jQuery);

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// city state auto complate plugin  
// you can use the call back to update any variables 
// ex: $("id").InitCityStateSearch(function(cityid){});
(function ($) {
    $.fn.InitCityStateSearch = function (angluarCallback) {
        //var cityActionURL = window.location.protocol + "//" + window.location.host + "/GeneralSetting/GetCities";
        var self = $(this);
        var cityActionURL = window.location.protocol + "//" + window.location.host + "/GeneralSetting/GetCities";
        var StateID = "#" + $(this).attr("data-state-id");
        var CityID = "#" + $(this).attr("data-city-id");
        var NotFoundID = "#" + $(this).attr("data-notfound-id");
        var SearchConditionID = "#" + $(this).attr("data-searchcondition-id");
        var AppendTo = "#" + $(this).attr("data-appendto-id");
        if (AppendTo == "#") AppendTo = "";
        //if (!self.val())
        //  return;
        if ($(this).val().length > 3) {
            $(SearchConditionID).hide();
        }
        $(NotFoundID).hide();
        $(this).autocomplete({
            appendTo: AppendTo,
            source: function (request, response) {

                var baseUrl = General_GetBaseUrl();
                var stateVal = $(StateID).val();
                stateVal = stateVal.replace("number:", "");
                if (!$(StateID).val()) {
                    alert("Please select home state");
                }
                if (!stateVal) {
                    alert("Please select home state");
                }
                else {
                    $.ajax({
                        url: baseUrl + "GeneralSetting/GetCities",
                        dataType: "json",
                        data: {
                            term: request.term,
                            //id: $(StateID).val()
                            id: stateVal
                        },
                        success: function (data) {

                            if (data.length == 0) {
                                $(NotFoundID).show();
                            }
                            else {
                                $(NotFoundID).hide();
                                response(data);
                            }
                        },
                        error: function (x, y, z) {
                            response(x);
                        }
                    });
                }
            },//'@Url.Action("GetCities", "GeneralSetting")',
            minLength: 3,
            //autoFocus: true,
            select: function (event, ui) {

                event.preventDefault();
                $(this).val(ui.item.label); // display the selected text
                $(CityID).val(parseInt(ui.item.id));
                $(CityID).trigger('input');
                if (angluarCallback)
                    angluarCallback($(CityID).val());
            },
            focus: function (event, ui) {

                event.preventDefault();
                $(this).val(ui.item.label);
            },
            change: function (event, ui) {
                if ($(this).val() == '') {
                    $(CityID).val('');
                    $(CityID).trigger('input');
                    if (angluarCallback)
                        angluarCallback($(CityID).val());
                }
            }

        });

        $(this).on("propertychange change keyup paste input", function () {
            if ($(this).val() == "") {
                $(CityID).val("");
                $(CityID).trigger('input');
                if (angluarCallback)
                    angluarCallback($(CityID).val());

            }
            if ($(this).val().length < 3) {
                $(SearchConditionID).show();
            }
            else {
                $(SearchConditionID).hide();
            }
        });
        $(this).blur(function () {
            var val = $(CityID).val();
            if (val == "") {
                $(NotFoundID).show();
            }
            else {
                $(NotFoundID).hide();
            }
            if (angluarCallback)
                angluarCallback(val);
        });
        $(StateID).change(function () {
            $(self).val("");
            $(CityID).val("");
            $(CityID).trigger('input');
            if (angluarCallback)
                angluarCallback($(CityID).val());
        });
    }
})(jQuery);

///////////////////////////////////////////////////////////////
// enum function

function General_GetEnumName(array, id) {
    for (var i = 0; i < array.length; i++) {
        var p = array[i];
        if (p.ID == id) {
            return p.Name;
        }
    }
    return "";
}


function General_GetEnumID(array, name) {
    for (var i = 0; i < array.length; i++) {
        var p = array[i];
        if (p.Name == name) {
            return p.ID;
        }
    }
    return "";
}

function General_GetEnumIDEx(array, Enum_name) {
    for (var i = 0; i < array.length; i++) {
        var p = array[i];
        if (p.EnumName == Enum_name) {
            return p.ID;
        }
    }
    return "";
}

function General_GetEnumNameEx(array, id) {
    for (var i = 0; i < array.length; i++) {
        var p = array[i];
        if (p.ID == id) {
            return p.EnumName;
        }
    }
    return "";
}

function General_GetCustomEnumObj(array, enumVal) {
    for (var i = 0; i < array.length; i++) {
        var p = array[i];
        if (p.ID == enumVal) {
            return p;
        }
    }
    return null;
}



function General_RedirectToPage(Controller_ActionName) {
    ShowWaitDialog();
    window.location.href = General_GetBaseUrl() + Controller_ActionName;
}

// get baseurl
function General_GetBaseUrl() {
    return window.location.protocol + "//" + window.location.host + "/";
}

/* this function identify who will perform the next action in a specific order 
  it take three arguments : 
   - enumsArray : take your enum as an array of (ID, Name ) 
   - Status : (int) of this order 
   - userType : false -> not client , true-> client
  it returns "lbl lbl-danger" or "lbl lbl-Info" or null that specify if the user will perform the next action or not*/
function General_WhoWillPerformNextAction(enumsArray, status, userType) {
    var className = "label label-default";
    var isNotClientNext;
    var isClientNext;

    var statusName = General_GetEnumName(enumsArray, status);
    isClientNext = (statusName == 'Waiting Customer Signature') || (statusName == 'Waiting Customer Confirmation') || (statusName == 'Invalid Signature') || (statusName == 'Waiting For Payment')

    if (!isClientNext)
        isNotClientNext = (statusName == 'Cancelled') || (statusName == 'Pending') || (statusName == 'Confirmed') || (statusName == 'Waiting For Payment') || (statusName == 'Payment Processed') || (statusName == 'Validating Signature') || (statusName == 'Processing');

    if (isClientNext) {
        className = (userType == true) ? className = "label label-danger" : "label label-info";
    }
    else if (isNotClientNext) {
        className = (userType == false) ? "label label-danger" : "label label-info";
    }

    return className;

}

// return status structure {ClassName, StatusName, WhoIsNext}
// whoIsNext 0 default, 1 client, 2 SD
function General_WhoWillPerformNextActionEx(enumsArray, status, userType) {
    var className = "label label-default";
    var isNotClientNext;
    var isClientNext;
    var whoisnext = 0;

    var statusName = General_GetEnumName(enumsArray, status);

    isClientNext = (statusName == 'Waiting Customer Signature') || (statusName == 'Waiting Customer Confirmation') || (statusName == 'Invalid Signature');

    if (!isClientNext)
        isNotClientNext = (statusName == 'Cancelled') || (statusName == 'Pending') || (statusName == 'Confirmed') || (statusName == 'Waiting For Payment') || (statusName == 'Payment Processed') || (statusName == 'Validating Signature') || (statusName == 'Processing');

    if (isClientNext) {
        className = (userType == true) ? className = "label label-danger" : "label label-info";
        whoisnext = 1;
    }
    else if (isNotClientNext) {
        className = (userType == false) ? "label label-danger" : "label label-info";
        whoisnext = 2;
    }

    //return className; 
    return { ClassName: className, StatusName: statusName, WhoIsNext: whoisnext };

}

//---------------- Print div -------------------------
function General_Print(divContents, stylePath) {
    var divToPrint = $(divContents).html();
    var popup_win = "toolbar=yes,location=no,directories=yes,menubar=yes,scrollbars=yes,width=800,height=600,left=100,top=100";
    var printWindow = window.open('', '', popup_win);

    stylePath = typeof stylePath !== 'undefined' ? stylePath : "/Content/FmsPrintStyle.css";

    printWindow.document.write('<html><head>' +
        '<link href="/Content/bootstrap.css" rel="stylesheet" />' +
        '<link href="' + stylePath + '" media="print" rel="stylesheet" />' +
        '<title></title>');
    printWindow.document.write('</head><body style="border:0"><div>');
    printWindow.document.write(divToPrint);
    printWindow.document.write('</div></body></html>');
    printWindow.document.close();
    printWindow.addEventListener('DOMContentLoaded', function () {
        printWindow.print();
    });
}
//--------------------------------------------------------------
/*
   return a list of status ( all status , client status and Sdstatus as a dictionnary : 
   AllStatus: all order status 
   ClientStatus:represent the client status 
   SDStatus:represent sd status 
*/
function General_GetOrderStatus(onSuccess, onFali) {
    var baseUrl = General_GetBaseUrl();
    var status = [];
    $.ajax({
        url: baseUrl + "GeneralOrder/GetOrderStatus",
        dataType: "json",
        data: {},
        sync: true,
        success: function (data) {
            onSuccess(data);
        },
        fail: function (error) {
            onFail(error);
        }
    });
    return status;
}

//---------------------------------------------------------------------------------------
/*
   Description: check if string start with a spacific string
   Usage: str.string_strwith(startwith_str);
*/

String.prototype.string_startWith = function (startwith_str) {
    if (!startwith_str)
        return false;
    var strLen = startwith_str.length;

    if (this.length < strLen)
        return false;

    var str2 = this.substring(0, strLen);
    if (str == str2)
        return true;
    else
        return false;

};

// display html div as block
// usage $("div-id").General_DisplayAsBlock();
(function ($) {
    $.fn.General_DisplayAsBlock = function () {
        this.css("display", "block");
    }
})(jQuery);


(function ($) {
    $.fn.General_DisplayAsInline = function () {
        this.css("display", "inline");
    }
})(jQuery);

//Show controls with fms-Angular class
function ShowAngularClass() {
    $(".fms-Angular").show();
}







/////////////////////////////////////////////////////////////////////////////////////////////
// history functions

// Type 0 Div
// Type 1 Function

// value string div or function call
// title is history state tile could be options
// param optional if exists should be passed to the function call in value

function General_PushState(Type, value, title, param) {
    var General_StateObj = {};
    General_StateObj.Type = Type;
    General_StateObj.Value = value;
    General_StateObj.Param = param;

    var Title = title;

    history.pushState({ page: General_StateObj }, Title);
}

function General_ReplaceState(Type, value, title, param) {
    var General_StateObj = {};
    General_StateObj.Type = Type;
    General_StateObj.Value = value;
    General_StateObj.Param = param;

    var Title = title;

    history.replaceState({ page: General_StateObj }, Title);
}

// this function should be called in window.onpopstate listener
function General_PopState(event) {
    if (event.state == null) {
        history.back();
    }
    else {
        var general_stateObj = event.state.page;
        if (general_stateObj.Type == 0) {
            General_ShowDiv("#" + general_stateObj.Value);
        }
        else if (general_stateObj.Type == 1) {
            var fnCall = general_stateObj.Value;
            if (general_stateObj.Param) {
                fnCall(general_stateObj.Param);
            }
            else
                fnCall();
        }

        else
            return;



    }

}


// thousand spartor
function General_numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
//function General_floatToInt(floatvalue)
//{
//    var intvalue = Math.floor(floatvalue);
//    var intvalue = Math.ceil(floatvalue);
//    var intvalue = Math.round(floatvalue);
//}

/*This Function Take :
- Array : that will search in 
- Property: string that represent property name 
- value: the value of property to mathch. 
and will return the index of the first object that will match, or -1 if not found */

function General_getIndexOf(array, prop, value) {
    if (array == null || prop == null)
        alert("You must send valid parameters to get index function. ");
    else {
        for (i = 0; i < array.length; i++)
            if (array[i][prop] === value)
                return i;

        return -1;
    }
};
///////////////////////////////////////////////////////////////////////////

// init button that scroll page to top
//to use this add this a tag at the end of main view to make sure it will not be overlaped 
// and put this function in the doc ready
//<a href="#" id="btn-gneral-back-to_top" class="back-to-top"><span class="fa fa-arrow-up fa-fw"></span></a>
function General_InitBackToTopBtn() {
    var offset = 220;
    var duration = 500;
    $(window).scroll(function () {
        if ($(this).scrollTop() > offset) {
            $('.back-to-top').fadeIn(duration);
        } else {
            $('.back-to-top').fadeOut(duration);
        }
    });

    $('.back-to-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, duration);
        return false;
    })
}


/////////////////////////////////////////////////
function General_formatDateTime(strin, sec) {
    if (!strin || strin == "")
        return "";

    var time = new moment(strin);
    if (time.isValid) {
        if (sec)
            return time.format("M/d/YYYY h:mm A");
        return time.format("M/d/YYYY h:mm:ss A");
    }

    return "";
}

function General_formatDate(strin, long) {
    if (!strin || strin == "")
        return "";

    var time = new moment(strin);
    if (time.isValid) {
        if (long)
            return time.format("LL");
        return time.format("M/d/YYYY");
    }

    return "";
}

function General_formatTime(strin, sec) {
    if (!strin || strin == "")
        return "";

    var time = new moment(strin);
    if (time.isValid) {
        if (sec)
            return time.format("LTS");
        return time.format("LT");
    }
    return "";
}

//return true if the device is not pc, for example phone or tablet
function General_isMobileOrPC() {
    var isMobile = false; //initiate as false
    // device detection
    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;

    return isMobile;
}

/*
Act As String.Format in C# 
*/
function General_FormatString(message, args) {

    if (message == undefined || message == null)
        return null;

    if (args == undefined || args == null)
        return message;

    if (args.length == 0)
        return message;

    for (i = 0; i < args.length ; i++)
        message = message.replace("{" + i + "}", args[i])

    return message;
}

//Genereate New GUID 
function General_GenerateGUID() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    };
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

///
var General_StateHistory = {
    SrcPage: null,
    PreviousState: null,
    Forward: false
}

General_PushState = function (SrcPage, stateName, stateUrl) {
    var level = !history.length ? history.length + 1 : 1;
    history.pushState({ page: stateName, level: level }, "", SrcPage + "#" + stateUrl);
    General_StateHistory.PreviousState = history.state;
    General_StateHistory.SrcPage = SrcPage;
}

//convert data uri to image
function General_dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], { type: mimeString });
}

// test object id
function General_IsNullOrEmptyObjectId(str) {
    return !General_ObjectIdHasValue(str);
}
function General_ObjectIdHasValue(str) {
    if (str != null)
        str = str.trim();
    if (!str || !str.trim() || str == '000000000000000000000000')
        return false;
    else
        return true;
}

function General_GuidHasValue(str) {
    if (str != null)
        str = str.trim();
    if (!str || !str.trim() || str == '00000000-0000-0000-0000-000000000000')
        return false;
    else
        return true;
}


function General_ConvertUTCToLocalString(strUTC) {


    var localTime = moment.utc(strUTC);

    if (localTime.isValid())
        return localTime.format();
    else
        return null;

}

function General_ConvertUTCToLocalMoment(strUTC) {
    if (!strUTC)
        return null;

    var localTime = moment.utc(strUTC);
    if (localTime.isValid())
        return localTime.toDate();
    else
        return null;

}


/////////////////////////////////////////////////
// string encoding
function General_EncodeStringToNumber(str) {
    str = str.trim();
    if (!str) return 0;
    //var number = "0x";
    var number = "";
    var length = str.length;
    for (var i = 0; i < length; i++)
        number += str.charCodeAt(i).toString(16);
    return number;
}

function General_DecodeNumberToString(number) {

    var string = "";
    //number = number.slice(2);
    var length = number.length;
    for (var i = 0; i < length;) {
        var code = number.slice(i, i += 2);
        string += String.fromCharCode(parseInt(code, 16));
    }
    return string;
}

///////////////// Contains method add it to an array as extension method 

Array.prototype.listContains = function (value) {
    for (var i in this)
        if (this[i] == value)
            return true;
    return false;
};