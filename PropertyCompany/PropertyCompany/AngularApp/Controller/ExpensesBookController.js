﻿{


    var clientMessages = {
        ServerUnknownError: "Oops, Server error, Please call support !",
        ConnectingServerError: "Error during connecting to server!",
        AddedSuccessfully:"تـم إضـافة البيانـات بنجـاح !!",
    };

    app.controller('ExpensesBookController', function ($scope, ExpensesBookServices, ShowProgressServices) {
        $scope.JExpenses = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function (serverParam) {
            $scope.JExpenses = angular.copy(serverParam.OperObject);
            $scope.NewExpenses = angular.copy(serverParam.OperObject);
            $scope.JExpenses.Date = '';
            $scope.NewExpenses.Date = '';
            $scope.ViewTitle = "دفتر مصروفات نثرية";
            $("#datePicker").datepicker();
            $scope.boollvar = false;
        };

        //========================Watches===============================

        //========================Basic Info============================

        $scope.saveData = function () {
            ShowWaitDialog();
            //$scope.boollvar = true;
            //ShowProgressServices.ShowProgress($scope.boollvar);
            ExpensesBookServices.saveData($scope.JExpenses).then(function (result) {
                if (result.statusText == "Ok") {
                    HideWaitDialog();
                    //$scope.boollvar = false;
                    //ShowProgressServices.ShowProgress($scope.boollvar);
                    //swal("", "تـم اضـافة البيـانات بنجاح", "success")
                    //if (!$scope.$$phase) $scope.$apply();
                    var message = General_FormatString(clientMessages.AddedSuccessfully);
                    ShowAndSetMessageBox("معلـومة", message);
                    $scope.JExpenses = $scope.NewExpenses;
                }
                else if (result.statusText == "fail") {
                    swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
                }
                if (!$scope.$$phase) $scope.$apply();
            }, function (error) {
                swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
            })
        };

        //========================Common ===============================

        $scope.testModal = function () {
            ShowWaitDialog();
            HideWaitDialog();
            var message = General_FormatString(clientMessages.AddedSuccessfully);
            ShowAndSetMessageBox("معلـومة", message);
        };
    })
}