﻿{


    var clientMessages = {
        ServerUnknownError: "Oops, Server error, Please call support !",
        ConnectingServerError: "Error during connecting to server!",
    };

    app.controller('CapitalExpendituresBookController', function ($scope, CapitalExpendituresBookServices, ShowProgressServices ) {
        $scope.CapExpendB = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function (serverParam) {
            $scope.CapExpendB = angular.copy(serverParam.OperObject);
            $scope.NewCapExpendB = angular.copy(serverParam.OperObject);
            $scope.CapExpendB.Date = '';
            $scope.NewCapExpendB.Date = '';
            $scope.ViewTitle = "دفتر مصروفات رأس مالية";
            $("#datePicker").datepicker();
            $scope.boollvar = false;
        };

        //========================Watches===============================

        //========================Basic Info============================

        $scope.saveData = function () {
            $scope.boollvar = true;
            ShowProgressServices.ShowProgress($scope.boollvar);
            CapitalExpendituresBookServices.saveData($scope.CapExpendB).then(function (result) {
                if (result.status == 200) {
                    $scope.boollvar = false;
                    ShowProgressServices.ShowProgress($scope.boollvar);
                    swal("", "تـم اضـافة البيـانات بنجاح", "success")

                }
                if (result.status != 200) {
                    $scope.boollvar = false;
                    ShowProgressServices.ShowProgress($scope.boollvar);
                    swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
                }
                $scope.CapExpendB = $scope.NewCapExpendB;
            }, function (error) {
                $scope.boollvar = false;
                ShowProgressServices.ShowProgress($scope.boollvar);
                swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
            })
        };

        //========================Common ===============================
    })
}
