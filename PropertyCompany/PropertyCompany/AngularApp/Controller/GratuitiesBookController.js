﻿{


    var clientMessages = {
        ServerUnknownError: "Oops, Server error, Please call support !",
        ConnectingServerError: "Error during connecting to server!",
    };

    app.controller('GratuitiesBookController', function ($scope, GratuitiesBookServices, ShowProgressServices) {
        $scope.GratBook = {};
        $scope.disableFlg = false;


        //========================Constructor===========================
        $scope.initBook = function (serverParam) {
            $scope.GratBook = angular.copy(serverParam.OperObject);
            $scope.NewGratBook = angular.copy(serverParam.OperObject);
            $scope.GratBook.Date = '';
            $scope.NewGratBook.Date = '';
            $scope.ViewTitle = "دفتر إكراميات";
            $("#datePicker").datepicker();
            $scope.boollvar = false;
        };

        //========================Watches===============================

        //========================Basic Info============================

        $scope.saveData = function () {
            $scope.boollvar = true;
            ShowProgressServices.ShowProgress($scope.boollvar);
            GratuitiesBookServices.saveData($scope.GratBook).then(function (result) {
                if (result.status == 200) {
                    $scope.boollvar = false;
                    ShowProgressServices.ShowProgress($scope.boollvar);
                    swal("", "تـم اضـافة البيـانات بنجاح", "success")

                }
                if (result.status != 200) {
                    $scope.boollvar = false;
                    ShowProgressServices.ShowProgress($scope.boollvar);
                    swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
                }
                $scope.GratBook = $scope.NewGratBook;
            }, function (error) {
                $scope.boollvar = false;
                ShowProgressServices.ShowProgress($scope.boollvar);
                swal("خطأ ...", "يبدو أن هناك مشكله ما , من فضلك تواصل مع الدعم الفنى !!", "error");
            })
        };

        //========================Common ===============================
    })
}
