﻿var app = angular.module("PropertyCompany", []);
app.factory('PageServices', function () {
    var title = 'الصفحة الرئيسية';
    return {
        title: function () { return title; },
        setTitle: function (newTitle) { title = newTitle }
    };
});
