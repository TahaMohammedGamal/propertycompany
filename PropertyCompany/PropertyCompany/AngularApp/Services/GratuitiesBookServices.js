﻿app.factory('GratuitiesBookServices', function ($http) {

    var GratuitiesBookServices = {};

    GratuitiesBookServices.saveData = function (GratBook) {
        return $http.post('/GratuitiesBook/SaveData', GratBook, { headers: { 'Content-Type': 'application/json' } });
    };

    return GratuitiesBookServices;
});
