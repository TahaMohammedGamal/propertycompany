﻿
app.factory('CapitalExpendituresBookServices', function ($http) {

    var CapitalExpendituresBookServices = {};

    CapitalExpendituresBookServices.saveData = function (CapExpendB) {
        return $http.post('/CapitalExpendituresBook/SaveData', CapExpendB, { headers: { 'Content-Type': 'application/json' } });
    };

    return CapitalExpendituresBookServices;
});
