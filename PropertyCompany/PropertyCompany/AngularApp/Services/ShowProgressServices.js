﻿var app = angular.module("PropertyCompany", []);
app.factory('ShowProgressServices', function () {
    return {
        ShowProgress: function (boollvar) {
            if (boollvar == true) {
                setTimeout(function () {
                    var modal = $('<div />');
                    modal.addClass("modal");
                    $('body').append(modal);
                    var loading = $(".loading");
                    loading.show();
                    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
                    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
                    loading.css({ top: top, left: left });
                }, 200);
            }
            if (boollvar == false) {
                var loading = $(".loading");
                loading.hide();
            }
        }
    };
});
