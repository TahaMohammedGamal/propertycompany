﻿using PropertyCompany.DAL;
using PropertyCompany.HelperClasses;
using PropertyCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyCompany.Controllers
{
    public class GratuitiesBookController : Controller
    {
        // GET: GratuitiesBook
        public ActionResult GratuitiesBook_Page()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new GratuitiesBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }

        public async Task<string> SaveData(GratuitiesBook GratBook)
        {
            var res = await GratBookDAL.AddNewOperatingGradBookAsync(GratBook);

            return Helper.ConvertDataToJson(res);
        }
    }
}