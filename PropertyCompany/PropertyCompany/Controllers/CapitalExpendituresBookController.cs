﻿using PropertyCompany.HelperClasses;
using PropertyCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PropertyCompany.DAL;


namespace PropertyCompany.Controllers
{
    public class CapitalExpendituresBookController : Controller
    {
        // GET: CapitalExpendituresBook
        public ActionResult CapitalExpendituresBookPage()
        {
            InClientBag();
            return View();
        }
        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new CapitalExpendituresBook());
            var bag = Helper.ConvertDataToJson(bagObj);
      
            ViewBag.ServerParams = bag;
        }

        public async Task<string> SaveData(CapitalExpendituresBook CapitExpBook)
        {
            var res = await CapitalExpBookDAL.AddNewCapitalExpendituresBookAsync(CapitExpBook);

            return Helper.ConvertDataToJson(res);
        }
    }
}