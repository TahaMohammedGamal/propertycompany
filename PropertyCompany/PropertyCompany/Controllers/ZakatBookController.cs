﻿using PropertyCompany.DAL;
using PropertyCompany.HelperClasses;
using PropertyCompany.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PropertyCompany.Controllers
{
    public class ZakatBookController : Controller
    {
        // GET: ZakatBook
        public ActionResult ZakatBook_Page()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new ZakatBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }
        public async Task<string> SaveData(ZakatBook zakatbook)
        {
            var res = await ZakatBookDAL.AddNewZakatBookAsync(zakatbook);
            return Helper.ConvertDataToJson(res);
        }
    }
}