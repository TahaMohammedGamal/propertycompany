﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PropertyCompany.DAL;
using PropertyCompany.HelperClasses;
using PropertyCompany.Models;

namespace PropertyCompany.Controllers
{
    public class OperatingExpensesBookController : Controller
    {
        // GET: OperatingExpensesBook
        public ActionResult Index()
        {
            InClientBag();
            return View();
        }

        private void InClientBag()
        {
            ClientBag bagObj = new ClientBag();

            bagObj += new ServerParam("OperObject", new ExpensesBook());
            var bag = Helper.ConvertDataToJson(bagObj);

            ViewBag.ServerParams = bag;
        }

        public async Task<string> SaveData(ExpensesBook JExpenses)
        {
            var res = await OperatingExeDAL.AddNewOperatingExeBookAsync(JExpenses);

            return Helper.ConvertDataToJson(res);
        }
    }
}